#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
 
#define Recv_Size 20*3*20
#define Send_Size 3*3*36+3

char local_map[20][20][2];
int turn_number = 0;

typedef struct {
  int p[25][2];
  int count;
} range;

range getRange(int x, int y, int is_first){
  int map[20][20] = {0};
  int i,j,k,l;
  range r;
  r.count=0;
  char t = is_first:'A':'B';
  char e = (t=='A')?'B':'A';
  for(i=0; i<25; i++){
    r.p[i][0] = r.p[i][1] = -1;
  }
  if(local_map[x][y][0] == t) map[x][y]=1;
  r.p[0][0] = x;
  r.p[0][1] = y;
  r.count++;
  for(i=0; i<3; i++){
    for(j=0; j<20; j++){
      for(k=0; k<20; k++){
        if(map[j][k]==1){
          if(j-1>=0 && map[j-1][k]==0 && local_map[j-1][k][0]!=e) map[j-1][k] = 1;
          if(j+1<20 && map[j+1][k]==0 && local_map[j+1][k][0]!=e) map[j+1][k] = 1;
          if(k-1>=0 && map[j][k-1]==0 && local_map[j][k-1][0]!=e) map[j][k-1] = 1;
          if(k+1<20 && map[j][k+1]==0 && local_map[j][k+1][0]!=e) map[j][k+1] = 1;
        }
      }
    }
  }
  for(j=0; j<20; j++){
    for(k=0; k<20; k++){
      if(map[j][k]==1 && local_map[j][k][0]==0) {
        r.p[r.count][0] = j;
        r.p[r.count][1] = k;
        r.count++;
      }
    }
  }
  return r;
}

void map_copy(char* recv_map){
  int i,j,k;
  for(i=0; i<20; i++){
    for(j=0; j<20; j++){
      for(k=0; k<2; k++){
        local_map[i][j][k] = recv_map[(i*20+j)*2+k];
      }
    }
  }
}

void print_map(void){
  int i,j;
  fprintf(stdout, "TURN:%5d\n", turn_number);
  for(i=0; i<20; i++){
    for(j=0; j<20; j++){
      fprintf(stdout, "%c%c", local_map[i][j][0], local_map[i][j][1]);
    }
    fprintf(stdout, "\n");
  }
}

int main(int argc, char** argv){

    int i,j,k;

    int is_first = 0;
    int is_local = 0;
    
    switch (argc) {
      case 3 :
      if(atoi(argv[2])==1){
        is_local = 1;
      }
      case 2 :
      if(atoi(argv[1])==1){
        is_first = 1;
      }
      default:
        break;
    }
    
    int my_port = is_first
             ? 20000
             : 20001;
    char* my_addr = is_local
                  ? "127.0.0.1"
                  : "192.168.11.15";

    int sd;  //ソケット作成用の変数
    struct sockaddr_in addr;  //サーバ接続用の変数
    char *recv_Map;  //受信データ格納用の変数
    recv_Map = (char *)malloc(sizeof(recv_Map)*Recv_Size);
    char *send_Msg; //送信データ格納用の変数
    send_Msg = (char *)malloc(sizeof(send_Msg)*(Send_Size+2));
    char *p;
    
    char buf[3000] = {'\0'};

    // IPv4 TCP のソケットを作成する
    if((sd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("socket");
        return -1;
    }
 
    // 送信先アドレスとポート番号を設定する
    addr.sin_family = AF_INET;
    addr.sin_port = htons(my_port);
    addr.sin_addr.s_addr = inet_addr(my_addr);

    // サーバ接続（TCP の場合は、接続を確立する必要がある）
    connect(sd, (struct sockaddr *)&addr, sizeof(struct sockaddr_in));
  
    // パケットを TCP で受信
    if(recv(sd, recv_Map, Recv_Size, 0)<0){
        perror("recv");
        return -1;
    }
    //printf("recv.DefaultMap\n%s\n",recv_Map);
    map_copy(recv_Map);
    print_map();
    switch(is_first) {
      while(1){
      if(is_first){
        goto FIRST;
      }
      default :
        // パケットを TCP で受信
        if(recv(sd, recv_Map, Recv_Size+1, 0)<0){
            perror("recv");
            return -1;
        }
        //改行を取り除いている
        p = strchr(recv_Map, '\n');  
        if(p != NULL) {  
          *p = '\0';  
        }
        //printf("recv.Map\n%s\n",recv_Map);
        map_copy(recv_Map);
        print_map();
      FIRST:
      case 1 :
        print_map();
        // 送信データの入力
        //strcpy( send_Msg, "999" );
        //scanf("%s",send_Msg);
        //fscanf(stdin, "%s", buf);
        //ai(is_first, buf);
        for(i=0; i<20;)
        sprintf(send_Msg, "%s", "999");
        strcat( send_Msg, "\n" );

        // パケットを TCP で送信
        if(send(sd, send_Msg, Send_Size+2, 0) < 0) {
            perror("send");
            return -1;
        }
        printf("send end\n");
        
        if(is_first){
          // パケットを TCP で受信
          if(recv(sd, recv_Map, Recv_Size+1, 0)<0){
              perror("recv");
              return -1;
          }
          //改行を取り除いている
          p = strchr(recv_Map, '\n');  
          if(p != NULL) {  
            *p = '\0';  
          }
          //printf("recv.Map\n%s\n",recv_Map);
          map_copy(recv_Map);
          print_map();
        }
      
       // パケットを TCP で受信
       if(recv(sd, recv_Map, Recv_Size+1, 0)<0){
           perror("recv");
           return -1;
       }
       //改行を取り除いている
       p = strchr(recv_Map, '\n');  
       if(p != NULL) {  
         *p = '\0';  
       }
       //printf("recv.Map\n%s\n",recv_Map);
       map_copy(recv_Map);
       print_map();
       
       // 無理やりつなぎなおし
       close(sd);
       printf("connect close\n");
       // IPv4 TCP のソケットを作成する
       if((sd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
         perror("socket");
         return -1;
       }
       // 送信先アドレスとポート番号を設定する
       addr.sin_family = AF_INET;
       addr.sin_port = htons(my_port);
       addr.sin_addr.s_addr = inet_addr(my_addr);
       connect(sd, (struct sockaddr *)&addr, sizeof(struct sockaddr_in));
       printf("connect reset\n");
       turn_number++;
      }
    } 

    // ソケットを閉じる
    close(sd);
 
    return 0;
}

