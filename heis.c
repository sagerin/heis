/* heis.c : Simple turn based strategy game */
/* author: Rin Sagehashi */
/* date: 2016 / 7 / 14 */


/* BLUE TEAM */
#define FRIEND_AI ApproachAI
/* RED TEAM */
#define ENEMY_AI ApproachAI
// getUser:手動
// RandomAI:取れる行動を完全ランダムでする
// AttackAI:攻撃をRandomAIの200倍くらい優先してする
// OnlyAtkAI:殴れる時に必ず殴るAI
// MustKillAI:殺せる時に必ず殺すAI
// ApproachAI:敵に近づくAI
// AvoidAI:敵から逃げるAI

typedef union{
  unsigned long long ull;
  char c[8];
} Range;

typedef struct{
  unsigned fromX:5;
  unsigned fromY:5;
  unsigned toX:5;
  unsigned toY:5;
  unsigned actX:5;
  unsigned actY:5;
  unsigned action:2; //0:無し 1:行動終了 2:降伏 3:無行動
} User;

typedef struct{
  int turn;
  int winner;
} Result;

typedef struct{
  int turnLimit;
  int first;
} Data;

/* ---- grobal variable ---- */
unsigned unit[2][2][20] = {0};
unsigned moveAble[20] = {0};

char *friendColor = "\e[34m"; // blue
char *enemyColor  = "\e[31m"; // red
char *moved       = " ";
char *unmoved     = "\e[4m ";
char *hp1         = "1\e[0m";
char *hp2         = "2\e[0m";
char *nothing     = " \e[0m";
char *selected    = "\e[47m";

int ANIME_ON = 1;
int GAME_LIMIT = 1;
int NETWORK_PLAY = 0;
int turnCount;

/* ---- prototype ---- */

void GameStart(int firstPlayer);
void display(void);
int moveCursor(int x, int y);
void moveUnit(User u, int pl);
Range checkRange(int x, int y, int pl);
User getUser(int pl);
Result PlayGame(int gameCount);
void PlayTurn(int pl);
int is_gameset(void);
int is_turnContinue(void); // ターン終了時に0を返す
User createUser(unsigned fx, unsigned fy, unsigned tx, unsigned ty, unsigned ax, unsigned ay);
User BasicAI(int pl);
int option(int argc, char* argv[]);
void printUnitAction(User u);
Result networkPlay(Data data);

#define is(x,y) ((unit[0][0][y]|unit[0][1][y]|unit[1][0][y]|unit[1][1][y])>>x&1)
#define in_board(x,y) (x>=0 && x<20 && y>=0 && y<20)
#define distance(bx,by,ax,ay) (abs(ax-bx)+abs(ay-by))
#define can_move(r) !(r.c[0]==0&&r.c[1]==0&&r.c[2]==0&&r.c[3]==0b0001000&&r.c[4]==0&&r.c[5]==0&&r.c[6]==0)

/* ---- main ---- */

int main(int argc, char* argv[]){
  Data data = {0, 0};
  int i;
  if(option(argc, argv)==1) return 0;
  if(NETWORK_PLAY==1){
    networkPlay(data);
    return 0;
  }
  for(i=0; i<GAME_LIMIT; i++){
    PlayGame(i);
  }
}

#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
/* ---- option ---- */
int option(int argc, char* argv[]){
  char *p, *target;
  int i,j;
  
  for(i=0; i<argc; i++){
    p=argv[i];
    if(*p == '-'){
      p++;
      if(*p == '-'){ // long name option
        p++;
        if(strcmp(p, "version")==0){
          printf("heis %.2f\n", 0.01);
          return 1;
        } else if(strcmp(p, "help")==0){
          printf("準備中\n");
          return 1;
        } else if(strcmp(p, "animation")==0){ //default
          ANIME_ON = 1;
        } else if(strcmp(p, "no-animation")==0){
          ANIME_ON = 0;
        } else if(strcmp(p, "network")==0){
          NETWORK_PLAY = 1;
        }
      } else {
        while(*p!='\0'){
          switch (*p) {
            case 'n' : ANIME_ON=0; break;
            case 'a' : ANIME_ON=1; break;
            case 'w' : NETWORK_PLAY=1; break;
            case 'r' : 
              if(*(p+1)=='\0' && i+1<argc){
                if(atoi(argv[i+1])>1) GAME_LIMIT = atoi(argv[i+1]);
              } else if(*(p+1)!='\0' && atoi(p+1)>1) {
                GAME_LIMIT = atoi(p+1);
              }
              break;
          }
          p++;
        }
        
      }
    } else {
    
    }
  }
  return 0;
}

/* ---- AI ---- */

User RandomAI(int pl){
  User u[30000];
  Range r;
  int en = (pl+1)%2;
  int fx,fy,tx,ty,ax,ay, top=0, old;
  int i,j;
  
  for(fx=0; fx<20; fx++){
    for(fy=0; fy<20; fy++){
      if(!(moveAble[fy]>>fx&1)) continue;
      r = checkRange(fx,fy,pl);
      for(tx=-3; tx<=3; tx++){
        for(ty=-3; ty<=3; ty++){
          if(!(r.c[3+ty]>>(3+tx)&1)) continue;
          u[top++] = createUser(fx,fy,fx+tx,fy+ty,21,21);
          for(ax=0; ax<20; ax++){
            for(ay=0; ay<20; ay++){
              if(!(distance(fx+tx,fy+ty,ax,ay)==1 && (unit[en][0][ay]|unit[en][1][ay])>>ax&1)) continue;
              u[top++] = createUser(fx,fy,fx+tx,fy+ty,ax,ay);
            }
          }
        }
      }
    }
  }
  if(top==0){
    u[0].action = 1;
    return u[0];
  }
  
  srand((unsigned)time(NULL));
  return u[rand()%top];
}
User AttackAI(int pl){
  User u[30000];
  Range r;
  int en = (pl+1)%2;
  int fx,fy,tx,ty,ax,ay, top=0, old;
  int i,j;
  
  for(fx=0; fx<20; fx++){
    for(fy=0; fy<20; fy++){
      if(!(moveAble[fy]>>fx&1)) continue;
      r = checkRange(fx,fy,pl);
        for(ty=-3; ty<=3; ty++){
        for(tx=-3; tx<=3; tx++){
          if(!(r.c[3+ty]>>(3+tx)&1)) continue;
          u[top++] = createUser(fx,fy,fx+tx,fy+ty,21,21);
          for(ax=0; ax<20; ax++){
            for(ay=0; ay<20; ay++){
              if(!(distance(fx+tx,fy+ty,ax,ay)==1 && (unit[en][0][ay]|unit[en][1][ay])>>ax&1)) continue;
              u[top++] = createUser(fx,fy,fx+tx,fy+ty,ax,ay);
            }
          }
        }
      }
    }
  }
  if(top==0){
    u[0].action = 1;
    return u[0];
  }
  // 以下評価部分
  old = top;
  for(i=0; i<old; i++){
    if(u[i].actX!=21) {
      for(j=0; j<200; j++)u[top++]=u[i];
    }
  }
  srand((unsigned)time(NULL));
  return u[rand()%top];
}

User OnlyAtkAI(int pl){
  User u[2000], atk[2000];
  Range r;
  int en = (pl+1)%2;
  int fx,fy,tx,ty,ax,ay, top=0, old;
  int i,j;
  
  for(fx=0; fx<20; fx++){
    for(fy=0; fy<20; fy++){
      if(!(moveAble[fy]>>fx&1)) continue;
      r = checkRange(fx,fy,pl);
        for(ty=-3; ty<=3; ty++){
        for(tx=-3; tx<=3; tx++){
          if(!(r.c[3+ty]>>(3+tx)&1)) continue;
          u[top++] = createUser(fx,fy,fx+tx,fy+ty,21,21);
          for(ax=0; ax<20; ax++){
            for(ay=0; ay<20; ay++){
              if(!(distance(fx+tx,fy+ty,ax,ay)==1 && (unit[en][0][ay]|unit[en][1][ay])>>ax&1)) continue;
              u[top++] = createUser(fx,fy,fx+tx,fy+ty,ax,ay);
            }
          }
        }
      }
    }
  }
  if(top==0){
    u[0].action = 1;
    return u[0];
  }
  
  srand((unsigned)time(NULL));
  
  // 以下評価部分
  old = top;
  top=0;
  for(i=0; i<old; i++){
    if(u[i].actX!=21) {
      atk[top++] = u[i];
    }
  }
  if(top!=0){
    return atk[rand()%top];
  }
  return u[rand()%old];
}

User MustKillAI(int pl){
  User u[2000], atk[2000], kill[2000];
  Range r;
  int en = (pl+1)%2;
  int fx,fy,tx,ty,ax,ay, top=0, old, ktop=0;
  int i,j;
  
  for(fx=0; fx<20; fx++){
    for(fy=0; fy<20; fy++){
      if(!(moveAble[fy]>>fx&1)) continue;
      r = checkRange(fx,fy,pl);
        for(ty=-3; ty<=3; ty++){
        for(tx=-3; tx<=3; tx++){
          if(!(r.c[3+ty]>>(3+tx)&1)) continue;
          u[top++] = createUser(fx,fy,fx+tx,fy+ty,21,21);
          for(ax=0; ax<20; ax++){
            for(ay=0; ay<20; ay++){
              if(!(distance(fx+tx,fy+ty,ax,ay)==1 && (unit[en][0][ay]|unit[en][1][ay])>>ax&1)) continue;
              u[top++] = createUser(fx,fy,fx+tx,fy+ty,ax,ay);
            }
          }
        }
      }
    }
  }
  if(top==0){
    u[0].action = 1;
    return u[0];
  }
  
  srand((unsigned)time(NULL));
  
  // 以下評価部分
  old = top;
  top=0;
  for(i=0; i<old; i++){
    if(u[i].actX!=21) {
      atk[top++] = u[i];
      if(unit[en][0][u[i].actY]>>(u[i].actX)&1){
        kill[ktop++] = u[i];
      }
    }
  }
  if(ktop!=0){
    return kill[rand()%ktop];
  }
  if(top!=0){
    return atk[rand()%top];
  }
  return u[rand()%old];
}

User ApproachAI(int pl){
  User u[2000], atk[2000], kill[2000], app[5000];
  Range r;
  int en = (pl+1)%2;
  int fx,fy,tx,ty,ax,ay;
  int top=0, old, ktop=0, atop=0, x, y;
  int i,j;
  
  for(fx=0; fx<20; fx++){
    for(fy=0; fy<20; fy++){
      if(!(moveAble[fy]>>fx&1)) continue;
      r = checkRange(fx,fy,pl);
        for(ty=-3; ty<=3; ty++){
        for(tx=-3; tx<=3; tx++){
          if(!(r.c[3+ty]>>(3+tx)&1)) continue;
          u[top++] = createUser(fx,fy,fx+tx,fy+ty,21,21);
          for(ax=0; ax<20; ax++){
            for(ay=0; ay<20; ay++){
              if(!(distance(fx+tx,fy+ty,ax,ay)==1 && (unit[en][0][ay]|unit[en][1][ay])>>ax&1)) continue;
              u[top++] = createUser(fx,fy,fx+tx,fy+ty,ax,ay);
            }
          }
        }
      }
    }
  }
  if(top==0){
    u[0].action = 1;
    return u[0];
  }
  
  srand((unsigned)time(NULL));
  
  // 以下評価部分
  old = top;
  top=0;
  for(i=0; i<old; i++){
    if(u[i].actX!=21) {
      atk[top++] = u[i];
      if(unit[en][0][u[i].actY]>>(u[i].actX)&1){
        kill[ktop++] = u[i];
      }
    }
  }
  if(ktop!=0){
    return kill[rand()%ktop];
  }
  if(top!=0){
    return atk[rand()%top];
  }
  for(i=0; i<old; i++){
    for(x=0; x<20; x++){
      for(y=0; y<20; y++){
        if(!((unit[en][0][y]|unit[en][1][y])>>x&1)) continue;
        if(distance(u[i].toX, u[i].toY, x, y)<=5) app[atop++] = u[i];
      }
    }
  }
  if(atop!=0){
    return app[rand()%atop];
  }
  return u[rand()%old];
}

User AvoidAI(int pl){
  User u[2000], atk[2000], kill[2000], app[5000];
  Range r;
  int en = (pl+1)%2;
  int fx,fy,tx,ty,ax,ay;
  int top=0, old, ktop=0, atop=0, x, y;
  int i,j;
  
  for(fx=0; fx<20; fx++){
    for(fy=0; fy<20; fy++){
      if(!(moveAble[fy]>>fx&1)) continue;
      r = checkRange(fx,fy,pl);
        for(ty=-3; ty<=3; ty++){
        for(tx=-3; tx<=3; tx++){
          if(!(r.c[3+ty]>>(3+tx)&1)) continue;
          u[top++] = createUser(fx,fy,fx+tx,fy+ty,21,21);
          for(ax=0; ax<20; ax++){
            for(ay=0; ay<20; ay++){
              if(!(distance(fx+tx,fy+ty,ax,ay)==1 && (unit[en][0][ay]|unit[en][1][ay])>>ax&1)) continue;
              u[top++] = createUser(fx,fy,fx+tx,fy+ty,ax,ay);
            }
          }
        }
      }
    }
  }
  if(top==0){
    u[0].action = 1;
    return u[0];
  }
  
  srand((unsigned)time(NULL));
  
  // 以下評価部分
  old = top;
  top=0;
  for(i=0; i<old; i++){
    if(u[i].actX!=21) {
      atk[top++] = u[i];
      if(unit[en][0][u[i].actY]>>(u[i].actX)&1){
        kill[ktop++] = u[i];
      }
    }
  }
  if(ktop!=0){
    return kill[rand()%ktop];
  }
  if(top!=0){
    return atk[rand()%top];
  }
  for(i=0; i<old; i++){
    for(x=0; x<20; x++){
      for(y=0; y<20; y++){
        if(!((unit[en][0][y]|unit[en][1][y])>>x&1)) continue;
        if(distance(u[i].toX, u[i].toY, x, y)>=4 && distance(u[i].toX, u[i].toY, x, y)<6) {
          app[atop++]=u[i];
          //goto end;
        }
      }
    }
    //app[atop++]=u[i];
    //end:
    //  ;
  }
  if(atop!=0){
    return app[rand()%atop];
  }
  return u[rand()%old];
}




/* ---- fanction ---- */
Result networkPlay(Data data){
  User user[36];
  Result result;
  char buf[400] = {'\0'};
  int from, to, atk;
  int i,j;
  GameStart(data.first);
  if(system("tput civis")==-1){
    exit(1);
  }
  while(1){
    if(ANIME_ON)display();
    if(is_gameset()) break;
    PlayTurn(0);
    turnCount++;
    if(ANIME_ON)display();
    if(is_gameset()) break;
    for(i=0; i<20; i++) moveAble[i] = unit[1][0][i] | unit[1][1][i];
    for(i=0; i<36; i++) user[i] = createUser(21,21,21,21,21,21);
    i=0;
    j=0;
    
    if(fgets(buf, 400, stdin)!=NULL){
      while(1){
        from = (buf[i]-48)*100 + (buf[i+1]-48)*10 + (buf[i+2]-48);
        i += 3;
        if(from==999) break;
        to = (buf[i]-48)*100 + (buf[i+1]-48)*10 + (buf[i+2]-48);
        i += 3;
        if(to==999) break;
        atk = (buf[i]-48)*100 + (buf[i+1]-48)*10 + (buf[i+2]-48);
        i += 3;
        if(atk==999) break;
        user[j++] = createUser(from%20, from/20, to%20, to/20, atk%20, atk/20);
      }
      for(i=0; i<j; i++){
        moveUnit(user[i], 1);
      }
    }    
    turnCount++;
  }
  if(system("tput cnorm")==-1){
    exit(1);
  }
}

Result PlayGame(int gameCount){
  int P1=0, P2=1;
  
  GameStart(0);
  if(system("tput civis")==-1){
    exit(1);
  }
  while(1){
    if(ANIME_ON)display();
    if(is_gameset()) break;
    PlayTurn(P1);
    turnCount++;
    if(ANIME_ON)display();
    if(is_gameset()) break;
    PlayTurn(P2); 
    turnCount++;
  }
  if(system("tput cnorm")==-1){
    exit(1);
  }
}

void PlayTurn(int pl){
  User u[36];
  int i,j;
  for(i=0; i<20; i++) moveAble[i] = unit[pl][0][i] | unit[pl][1][i];
  for(i=0; i<36; i++) u[i] = createUser(21,21,21,21,21,21);
  i=0;
  while(is_turnContinue()){
    if(pl) u[i] = ENEMY_AI(pl); // red
    else u[i] = FRIEND_AI(pl);   // blue
    if(u[i].action == 1){
      printf("行動終了\n");
      break;
    }
    if(u[i].action == 3) continue;
    moveUnit(u[i], pl);
    if(ANIME_ON)printf("\e[23;1H%d:%2d %2d %2d %2d %2d %2d", pl,u[i].fromX, u[i].fromY, u[i].toX, u[i].toY, u[i].actX, u[i].actY);
    i++;
  }
  if(ANIME_ON) return;
  for(j=0; j<36; j++){
    if(j<i)printUnitAction(u[j]);
    else printf("999999999");
  }
  printf("999\n");
}

User createUser(unsigned fx, unsigned fy, unsigned tx, unsigned ty, unsigned ax, unsigned ay){
  User u = {fx,fy,tx,ty,ax,ay};
  return u;
}

void printUnitAction(User u){
    if(u.fromX > 20 || u.fromX > 20 || u.toX > 20 || u.toY > 20) return;
    else printf("%03d%03d", u.fromX + u.fromY*20, u.toX + u.toY*20);
    if(u.actX > 20 || u.actY > 20) printf("%03d", u.toX + u.toY*20);
    else printf("%03d", u.actX + u.actY*20);
}

int is_turnContinue(void){
  unsigned b=0, w0=0, w1=0;
  int i;
  for(i=0; i<20; i++) b |= moveAble[i];
  for(i=0; i<20; i++) w0 |= (unit[0][0][i]|unit[0][1][i]);
  for(i=0; i<20; i++) w1 |= (unit[1][0][i]|unit[1][1][i]);
  return b!=0 ;//&& w1!=0 && w0!=0;
}

Range checkRangeSub(Range w, Range m, int ix, int iy, int n){
  if(n>3 || !(m.c[3+iy]>>(3+ix)&1)) return w;
  w.c[3+iy] |= 1<<(3+ix); 
  w = checkRangeSub(w, m, ix+1, iy, n+1);
  w = checkRangeSub(w, m, ix-1, iy, n+1);
  w = checkRangeSub(w, m, ix, iy+1, n+1);
  w = checkRangeSub(w, m, ix, iy-1, n+1);
  return w;
}

Range checkRange(int x, int y, int pl){
  Range moveRange, waitArea;
  int en = (pl+1)%2;
  int i,j;
  moveRange.ull = waitArea.ull = 0;
  
  if(!((unit[pl][0][y]|unit[pl][1][y])>>x&1)) return waitArea;
  for(i=-3; i<=3; i++){
    for(j=-3; j<=3; j++){
      moveRange.c[3+i] |= (   abs(i)+abs(j)<=3 
                       && in_board(x+j,y+i) 
                       && ~(unit[en][0][y+i]|unit[en][1][y+i])>>(x+j)&1
                       && (abs(i)+abs(j)==3&&(unit[pl][0][y+i]|unit[pl][1][y+i])>>(x+j)&1?0:1))<<(3+j);
      waitArea.c[3+i] |= ((unit[pl][0][y+i]|unit[pl][1][y+i])>>(x+j)&1)<<(3+j);
    }
  }
  waitArea.ull = ~waitArea.ull & checkRangeSub(waitArea,moveRange,0,0,0).ull | 1<<27;
  for(i=0; i<7; i++){
    for(j=0; j<7; j++){
      if(waitArea.c[i]>>j&1) waitArea.c[7]++; 
    }
  }
  return waitArea;
}

void moveUnit(User u, int pl){
  int en = (pl+1)%2;
  if(   !(moveAble[u.fromY]>>u.fromX&1) 
     || abs(u.toX-u.fromX)+abs(u.toY-u.fromY) > 3 
     || ~checkRange(u.fromX,u.fromY,pl).c[3+u.toY-u.fromY]>>(3+u.toX-u.fromX)&1
     || (is(u.toX,u.toY) && !(u.fromX==u.toX && u.fromY==u.toY))){
    return;
    }
  if(u.fromX==u.toX && u.fromY==u.toY){
    moveAble[u.fromY] &= ~(1<<u.fromX);
  } else {
    unit[pl][0][u.toY] |= (unit[pl][0][u.fromY]>>u.fromX&1)<<u.toX;
    unit[pl][0][u.fromY] &= ~(1<<u.fromX);
    unit[pl][1][u.toY] |= (unit[pl][1][u.fromY]>>u.fromX&1)<<u.toX;
    unit[pl][1][u.fromY] &= ~(1<<u.fromX);
    moveAble[u.fromY] &= ~(1<<u.fromX);
  }
  if(abs(u.actX-u.toX)+abs(u.actY-u.toY)==1 && in_board(u.actX,u.actY)){
    if(unit[en][1][u.actY]>>u.actX&1){
      unit[en][1][u.actY] &= ~(1<<u.actX);
      unit[en][0][u.actY] |= 1<<u.actX;
    } else if(unit[en][0][u.actY]>>u.actX&1){
      unit[en][0][u.actY] &= ~(1<<u.actX);
    }
  }
}

User getUser(int pl){
  User u;
  char buf[80];
  int fx=31,fy=31,tx=31,ty=31,ax=31,ay=31,act=0;
  if(fgets(buf, 80, stdin)==NULL){
    return u;
  }
  sscanf(buf, "%d %d %d %d %d %d %d", &fx, &fy, &tx, &ty, &ax, &ay, &act);
  u.fromX=fx; u.fromY=fy; u.toX=tx; u.toY=ty; u.actX=ax; u.actY=ay, u.action=act;
  return u;
}

void display(void){
  int i,j;
  int blueUnit=0, redUnit=0, blueHP=0, redHP=0; 
  for(i=0; i<20; i++){
    for(j=0; j<20; j++){
      moveCursor(i,j);
      //if(is(j,i)){ fprintf(stdout,"oo");} else{printf("  ");}continue;
      if(unit[0][0][i]>>j&1 || unit[0][1][i]>>j&1) fprintf(stdout,"%s", friendColor);
      else if(unit[1][0][i]>>j&1 || unit[1][1][i]>>j&1) fprintf(stdout, "%s", enemyColor);
      if(moveAble[i]>>j&1) fprintf(stdout, "%s", unmoved);
      else fprintf(stdout, "%s", moved);
      if(unit[0][0][i]>>j&1 || unit[1][0][i]>>j&1) fprintf(stdout, "%s", hp1);
      else if(unit[0][1][i]>>j&1 || unit[1][1][i]>>j&1) fprintf(stdout, "%s", hp2);
      else fprintf(stdout, "%s", nothing);
      
           if(unit[0][0][i]>>j&1) {blueUnit++; blueHP+=1;}
      else if(unit[0][1][i]>>j&1) {blueUnit++; blueHP+=2;}
      else if(unit[1][0][i]>>j&1) {redUnit++;  redHP+=1; }
      else if(unit[1][1][i]>>j&1) {redUnit++;  redHP+=2; }
    } 
  }
  fprintf(stdout, "\e[22;1HfromX fromY toX toY atkX atkY turn:%d BLUE: %2d/%2d RED: %2d/%2d\n\e[23;1H\e[0K", turnCount,blueHP,blueUnit,redHP,redUnit);
}

int moveCursor(int x, int y){
  if(x<0 || x>19 || y<0 || y>19) return 0;
  fprintf(stdout, "\e[%d;%dH", 2+x, 4+y*3);
  return 1;
}

int is_gameset(void){
  int i,w0=0,w1=0;
  for(i=0; i<20; i++) w0 |= unit[0][0][i]|unit[0][1][i];
  for(i=0; i<20; i++) w1 |= unit[1][0][i]|unit[1][1][i];
  return w0 == 0 || w1 == 0; 
}
void GameStart(int firstPlayer){
  int i,j;
  turnCount = 0;
  for(i=0; i<6; i++){
    for(j=14; j<20; j++){
      unit[0][1][j] |= 1<<i;
      unit[1][1][i] |= 1<<j;
    }
  }
  if(ANIME_ON)fprintf(stdout, "\e[2J\e[1;1H   ");
  if(ANIME_ON)for(i=0; i<20; i++) fprintf(stdout, "%2d ", i);
  if(ANIME_ON)for(i=0; i<20; i++) {
    fprintf(stdout, "\e[%d;0H%2d", i+2, i);
    for(j=0; j<20; j++) fprintf(stdout, "|  ");
    putchar('|');
  }
  if(ANIME_ON)putchar('\n');
  //for(i=0; i<20; i++) moveAble[i] = unit[firstPlayer][0][i] | unit[firstPlayer][1][i];
}
